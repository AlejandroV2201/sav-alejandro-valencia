import Data.ArgsManager;
import Data.SavManager;

public class SAV {

  //Examples:
  //java SAV a=i t=n o=az in=r r=20 s=140 c=console
  //java SAV a=b t=n o=az in=m v=“0,-1,3,1,10” s=500 c=swing
  //java SAV a=s t=c o=za in=m v=“b,B,a,Z” s=200 c=horizontal
  public static void main(String[] args) {
    ArgsManager savedData = new ArgsManager(args);
    if (savedData.isThrewException()) return;
    SavManager manager = new SavManager(savedData);
    manager.start();
  }
}