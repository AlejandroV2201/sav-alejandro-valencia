package Utils;

import java.util.ArrayList;
import java.util.List;

public final class Utils {
  private static Utils instance = null;

  public Utils() {
  }

  public static Utils getInstance() {
    if (instance == null) {
      instance = new Utils();
    }
    return instance;
  }

  public List<Object> arrayToIntList(int[] array) {
    List<Object> list = new ArrayList<>();
    for (int num : array) {
      list.add(num);
    }
    return list;
  }

  public List<Object> arrayToCharList(char[] array) {
    List<Object> list = new ArrayList<>();
    for (char ch : array) {
      list.add(ch);
    }
    return list;
  }

  public int castObjectToInt(Integer num) {
    return (int)(num);
  }
  public int castObjectToInt(Character ch) {
    return (int)(ch.toString().charAt(0));
  }
}
