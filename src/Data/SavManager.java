package Data;

import Algorithms.Algorithm;
import Algorithms.BubbleOrder;
import Algorithms.InsertionOrder;
import Algorithms.RandomOrder;
import Algorithms.SelectionOrder;
import Algorithms.StalinOrder;
import Graphics.ConsoleManager;
import Graphics.LibRender;

public class SavManager {
  ArgsManager data;
  Algorithm<Object> orderType;
  public SavManager(ArgsManager data) {
    this.data = data;
  }

  public void start() {
    setAlgorithm();
    if (data.getRender().equals("console")) {
      printUsingConsole();
    } else {
      printUsingLib();
    }
  }

  private void setAlgorithm() {
    boolean ascendant = data.isAscendant();
    switch (data.getAlgorithm()) {
      case ('s') -> orderType = new SelectionOrder<>(data.getList(), ascendant);
      case ('b') -> orderType = new BubbleOrder<>(data.getList(), ascendant);
      case ('i') -> orderType = new InsertionOrder<>(data.getList(), ascendant);
      case ('t') -> orderType = new StalinOrder<>(data.getList(), ascendant);
      case ('r') -> orderType = new RandomOrder<>(data.getList(), ascendant);
      default -> {
        System.out.println("This algorithm isn't implemented yet, using insertion algorithm");
        orderType = new InsertionOrder<>(data.getList(), ascendant);
      }
    }
  }

  private void printUsingLib() {
    if (data.getTypeArray() == 'n') {
      LibRender<Integer> libRender = new LibRender<>(data, orderType);
      libRender.printList();
    } else if (data.getTypeArray() == 'c') {
      LibRender<Character> libRender = new LibRender<>(data, orderType);
      libRender.printList();
    }
  }

  private void printUsingConsole() {
    ConsoleManager consoleManager = new ConsoleManager(data, orderType);
    consoleManager.printList();
  }

}
