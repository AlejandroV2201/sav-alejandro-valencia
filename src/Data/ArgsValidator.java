package Data;

import java.util.ArrayList;
import java.util.Arrays;

public class ArgsValidator {

  private boolean threwException = false;
  private final ArgsManager data;
  private final ArrayList<Character> ALGORITHMS = new ArrayList<>(Arrays.asList('b', 'i', 's', 'r', 't'));

  public ArgsValidator(ArgsManager data) {
    this.data = data;
  }

  public boolean validateValues(String[] args) {
    missingData(args);
    invalidData();
    return threwException;
  }

  private void missingData(String[] args) {
    if (data.getAlgorithm() == '\0') {
      throwMyException("Algorithm isn't defined.");
    }

    if (data.getTypeArray() == '\0') {
      throwMyException("Type of Array isn't defined.");
    }

    if (data.getOrder().isEmpty()) {
      throwMyException("Order isn't defined.");
    }

    if (data.getTypeInput() == '\0') {
      throwMyException("Type of Input isn't defined.");
    }

    if (data.getTime() == 0) {
      throwMyException("Time isn't defined.");
    }

    if (data.getTypeInput() == 'r' && data.getRandomAmount() == 0) {
      throwMyException("Random array length isn't defined.");
    }

    if (data.getTypeInput()== 'm' && data.getList().size() == 0/*(intList.size() == 0 && charList.size() == 0)*/) {
      throwMyException("Array isn't defined.");
    }

    if (data.getTypeInput() == 'm' && data.getRandomAmount() != 0) {
      throwMyException("Can't assign random amount to manual input.");
    }

    for (String arg : args) {
      if (data.getTypeInput() == 'r' && arg.charAt(0) == 'v') {
        throwMyException("Can't assign manual values with random input configuration.");
      }
    }

    if (data.getRender().isEmpty()) {
      data.setRender("Swing");
    }
  }

  private void invalidData() {
    if (data.getTime() < 100 ||data.getTime() > 1000) {
      throwMyException("Time outside 100 - 1000 ms range.");
    }

    if (data.getTypeArray() != 'c' && data.getTypeArray() != 'n') {
      throwMyException("Invalid Type of Array");
    }

    if (!ALGORITHMS.contains(data.getAlgorithm())) {
      throwMyException("Invalid Algorithm");
    }

    if (!data.getOrder().equalsIgnoreCase("az") && !data.getOrder().equalsIgnoreCase("za")) {
      throwMyException("Invalid Order");
    }

    if (Character.toLowerCase(data.getTypeInput()) != 'r' &&
        Character.toLowerCase(data.getTypeInput()) != 'm') {
      throwMyException("Invalid Type of Input");
    }

    if (data.getTypeInput() == 'r' &&
        (data.getRandomAmount() < 1 || data.getRandomAmount() > 40)) {
      throwMyException("Invalid Random Amount");
    }
  }

  private void throwMyException(String message) {
    try {
      throw new Exception(message);
    } catch (Exception e) {
      threwException = true;
      e.printStackTrace();
    }
  }

}
