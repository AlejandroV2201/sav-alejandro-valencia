package Data;

import Utils.Utils;
import java.util.List;

public class ArgsParser {

  public ArgsParser() {
  }

  public char parseAlgorithm(String arg) {
    String[] argArray = arg.split("=");
    return argArray[1].toLowerCase().charAt(0);
  }

  public char parseTypeArray(String arg) {
    String[] argArray = arg.toLowerCase().split("=");
    return (argArray[1].charAt(0));
  }

  public String parseOrder(String arg) {
    String[] argArray = arg.toLowerCase().split("=");
    return (argArray[1]);
  }

  public String parseRender(String arg) {
    String[] argArray = arg.toLowerCase().split("=");
    return (argArray[1]);
  }

  public char parseTypeInput(String arg) {
    String[] argArray = arg.toLowerCase().split("=");
    return (argArray[1].charAt(0));
  }

  public int parseRandomAmount(String arg) {
    String[] argArray = arg.toLowerCase().split("=");
    return (Integer.parseInt(argArray[1]));
  }

  public List<Object> parseIntArray(String arg) {
    String[] argArray = arg.split("=");
    argArray[1] = argArray[1].substring(1, argArray[1].length() - 1);
    String[] intArgArray = argArray[1].split(",");
    int[] result = new int[intArgArray.length];
    int counter = 0;
    for (String index : intArgArray) {
      if (!index.equals(",")) {
        result[counter] = Integer.parseInt(index);
        counter++;
      }
    }
    return (Utils.getInstance().arrayToIntList(result));
  }

  public List<Object> parseCharArray(String arg) {
    String[] argArray = arg.split("=");
    argArray[1] = argArray[1].substring(1, argArray[1].length() - 1);
    String[] charArgArray = argArray[1].split(",");
    char[] result = new char[charArgArray.length];
    int counter = 0;
    for (String index : charArgArray) {
      if (!index.equals(",")) {
        result[counter] = index.charAt(0);
        counter++;
      }
    }
    return (Utils.getInstance().arrayToCharList(result));
  }

  public int parseTime(String arg) {
    String[] argArray = arg.toLowerCase().split("=");
    return (Integer.parseInt(argArray[1]));
  }
}
