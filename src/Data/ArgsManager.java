package Data;

import Utils.Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.random.RandomGenerator;

public class ArgsManager {

  private final RandomGenerator randomGenerator = RandomGenerator.getDefault();
  private char algorithm; //
  private char typeArray;//
  private String order;//
  private char typeInput;//
  private int randomAmount;//
  private List<Object> list = new ArrayList<>();
  private int time;//
  private boolean threwException = false;
  private boolean isAscendant;
  private String render = "";
  public ArgsManager(String[] args) {
    ArgsParser argsParser = new ArgsParser();
    for (String arg : args) {
      switch (arg.toLowerCase().charAt(0)) {
        case ('a'):
          setAlgorithm(argsParser.parseAlgorithm(arg));
          break;
        case ('t'):
          setTypeArray(argsParser.parseTypeArray(arg));
          break;
        case ('o'):
          setOrder(argsParser.parseOrder(arg));
          break;
        case ('i'):
          setTypeInput(argsParser.parseTypeInput(arg));
          break;
        case ('r'):
          break;
        case ('v'):
          break;
        case ('s'):
          setTime(argsParser.parseTime(arg));
          break;
        case ('c'):
          setRender(argsParser.parseRender(arg));
          break;
        default:
          System.out.println(arg.toLowerCase().charAt(0) + " is not a valid argument!");
      }
    }
    for (String arg : args) {
      switch (arg.toLowerCase().charAt(0)) {
        case ('r') -> {
          setRandomAmount(argsParser.parseRandomAmount(arg));
          if (getTypeInput() == 'r') {
            makeRandomArray(getTypeArray(), getRandomAmount());
          }
        }
        case ('v') -> {
          if (getTypeArray() == 'n') {
            setList(argsParser.parseIntArray(arg));
          } else {
            setList(argsParser.parseCharArray(arg));
          }
        }
      }
    }
    validateValues(args);
  }

  private void validateValues(String[] args) {
    ArgsValidator validator = new ArgsValidator(this);
    threwException = validator.validateValues(args);
  }

  private void makeRandomArray(char type, int length) {
    if (type == 'n') {
      int[] result = new int[length];
      for (int index = 0; index < result.length; index++) {
        result[index] = randomGenerator.nextInt(-1000, 1001);
      }
      setList(Utils.getInstance().arrayToIntList(result));
    } else {
      char[] result = new char[length];
      for (int index = 0; index < result.length; index++) {
        if (randomGenerator.nextInt(0,2) == 0) {
          result[index] = (char)(randomGenerator.nextInt(65, 91));
          // 65 - 90 = A - Z
        } else {
          result[index] = (char)(randomGenerator.nextInt(97, 123));
          // 97 - 122 = a - z
        }
      }
      setList(Utils.getInstance().arrayToCharList(result));
    }
  }


  public char getAlgorithm() {
    return algorithm;
  }

  public void setAlgorithm(char algorithm) {
    this.algorithm = algorithm;
  }

  public char getTypeArray() {
    return typeArray;
  }

  public void setTypeArray(char typeArray) {
    this.typeArray = typeArray;
  }

  public String getOrder() {
    return order;
  }

  public void setOrder(String order) {
    this.order = order;
    isAscendant = order.equalsIgnoreCase("az");
  }

  public char getTypeInput() {
    return typeInput;
  }

  public void setTypeInput(char typeInput) {
    this.typeInput = typeInput;
  }

  public int getRandomAmount() {
    return randomAmount;
  }

  public void setRandomAmount(int randomAmount) {
    this.randomAmount = randomAmount;
  }

  public <T> List<T>  getList() {
    if (getTypeArray() == 'n') {
      List<Integer> integerList = new ArrayList<>();
      for (Object o : list) {
        integerList.add((Integer) o);
      }
      return (List<T>) integerList;
    } else {
      List<Character> characterList = new ArrayList<>();
      for (Object o : list) {
        characterList.add((Character) o);
      }
      return (List<T>) characterList;
    }
  }

  public <T> T getListClass() {
    Character a = 'a';
    Integer b = 1;
    return (typeArray == 'n') ? (T) a.getClass() : (T) b.getClass();
  }

  public void setList(List<Object> list) {
    this.list = list;
  }

  public int getTime() {
    return time;
  }

  public void setTime(int time) {
    this.time = time;
  }

  public boolean isThrewException() {
    return threwException;
  }

  public boolean isAscendant() {
    return isAscendant;
  }

  public String getRender() {
    return render;
  }

  public void setRender(String render) {
    this.render = render;
  }

}
