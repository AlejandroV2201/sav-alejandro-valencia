package Algorithms;

import java.util.ArrayList;
import java.util.List;

public class InsertionOrder <T> extends Algorithm<T> {

  private final String name = "Insertion Sort";
  private int index = 0;
  private List<T> list;
  private List<Integer> comparedValues = new ArrayList<>();

  public InsertionOrder(List<T> list, boolean ascendant) {
    super(list, ascendant);
    this.list = super.getList();
  }

  @Override
  public void orderList() {
    T aux;
    comparedValues.clear();
    if ((super.cast(list.get(index)) > super.cast(list.get(index + 1)) && super.getAscendant())
    || ((super.cast(list.get(index)) < super.cast(list.get(index + 1))) && !super.getAscendant())) {
      aux = list.get(index);
      list.set(index, list.get(index + 1));
      list.set(index + 1, aux);
    }
    comparedValues.add(index);
    comparedValues.add(index + 1);
    index++;
    super.setComparedValues(comparedValues);
    super.setList(list);
    if (index + 1 == list.size()) {
      index = 0;
    }
  }

  @Override
  public void printPresentation() {
    System.out.println("Ordering with Insertion Sort Algorithm: ");
  }

  @Override
  public String getName() {
    return name;
  }
}
