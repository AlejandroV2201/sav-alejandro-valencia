package Algorithms;

import java.util.ArrayList;
import java.util.List;

public class BubbleOrder <T> extends Algorithm<T> {

  private final String name = "Bubble Sort";
  private int index = 0;
  private List<T> list;
  private List<Integer> comparedValues = new ArrayList<>();

  public BubbleOrder(List<T> list, boolean ascendant) {
    super(list, ascendant);
    this.list = super.getList();
  }

  @Override
  public void orderList() {
    T aux;
    comparedValues.clear();
    if ((super.cast(list.get(index)) > super.cast(list.get(index + 1)) && super.getAscendant())
    || (super.cast(list.get(index)) < super.cast(list.get(index + 1)) && !super.getAscendant())) {
      aux = list.get(index);
      list.set(index, list.get(index + 1));
      list.set(index + 1, aux);
    }
    comparedValues.add(index);
    comparedValues.add(index + 1);
    if (index == list.size() - 2) {
      index = 0;
    } else {
      index++;
    }
    super.setList(list);
    super.setComparedValues(comparedValues);
  }

  @Override
  public void printPresentation() {
    System.out.println("Ordering with Bubble Sort Algorithm: ");
  }

  @Override
  public String getName() {
    return name;
  }

}
