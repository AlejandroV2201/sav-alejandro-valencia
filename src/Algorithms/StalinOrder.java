package Algorithms;

import java.util.ArrayList;
import java.util.List;

public class StalinOrder <T> extends Algorithm<T> {

  private final String name = "Stalin Sort";
  private int index = 0;
  private int indexToDelete;
  private boolean sovietFlag = false;
  private List<T> list;
  private List<Integer> comparedValues = new ArrayList<>();

  public StalinOrder(List<T> list, boolean ascendant) {
    super(list, ascendant);
    this.list = super.getList();
  }

  @Override
  public void orderList() {
    if (sovietFlag) {
      list.remove(indexToDelete);
    }
    comparedValues.clear();
    if (list.size() == 1) {
      return;
    }
    if ((super.cast(list.get(index)) > super.cast(list.get(index + 1)) && super.getAscendant())
        || ((super.cast(list.get(index)) < super.cast(list.get(index + 1))) && !super.getAscendant())) {
      comparedValues.add(index);
      indexToDelete = index;
      index = 0;
      sovietFlag = true;
    }
    super.setComparedValues(comparedValues);
    super.setList(list);
    if (index + 2 >= list.size() - 1) {
      index = 0;
    } else {
      index++;
    }
  }

  @Override
  public void printPresentation() {
    System.out.println("Order following Stalin's rules! ");
  }

  @Override
  public String getName() {
    return name;
  }
}
