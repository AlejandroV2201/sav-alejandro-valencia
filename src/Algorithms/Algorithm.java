package Algorithms;

import java.util.ArrayList;
import java.util.List;

public class Algorithm <T> implements IAlgorithm  {

  private final String name = "Undefined Algorithm";
  private List<T> list = new ArrayList<>();
  private List<Integer> comparedValues = new ArrayList<>();
  private boolean isOrdered = false;
  private final boolean ascendant;
  public Algorithm(List<T> list, boolean ascendant){
    this.ascendant = ascendant;
    this.list.addAll(list);
  }
  @Override
  public void orderList() {
  }

  @Override
  public void checkOrder() {
    isOrdered = true;
    for (int index = 0; index < list.size() - 1; index++) {
      if (ascendant && cast(list.get(index)) > cast(list.get(index + 1))) {
        isOrdered = false;
        break;
      } else if (!ascendant && cast(list.get(index)) < cast(list.get(index + 1))) {
        isOrdered = false;
        break;
      }
    }
  }

  @Override
  public void printPresentation() {
    System.out.println("This algorithm doesn't have a presentation yet.");
  }

  protected int cast(Object obj) {
    return obj.hashCode();
  }

  public boolean isOrdered() {
    return isOrdered;
  }

  public List<Integer> getComparedValues() {
    return comparedValues;
  }

  public List<T> getList() {
    return list;
  }

  protected void setList(List<T> list) {
    this.list = list;
  }

  protected boolean getAscendant() {
    return ascendant;
  }

  protected void setComparedValues(List<Integer> list) {
    this.comparedValues = list;
  }

  public String getName() {
    return name;
  }
}
