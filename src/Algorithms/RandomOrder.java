package Algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.random.RandomGenerator;

public class RandomOrder <T> extends Algorithm<T> {

  private final String name = "Random Sort";
  private List<T> list;
  private List<Integer> comparedValues = new ArrayList<>();
  private RandomGenerator randomGenerator = RandomGenerator.getDefault();
  public RandomOrder(List<T> list, boolean ascendant) {
    super(list, ascendant);
    this.list = super.getList();
  }

  @Override
  public void orderList() {
    Collections.shuffle(list);
    comparedValues.add(randomGenerator.nextInt(0, list.size()));
  }

  @Override
  public void printPresentation() {
    System.out.println("Ordering randomly!");
  }

  @Override
  public String getName() {
    return name;
  }
}
