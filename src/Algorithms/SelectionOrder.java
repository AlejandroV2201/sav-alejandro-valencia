package Algorithms;

import java.util.ArrayList;
import java.util.List;

public class SelectionOrder <T> extends Algorithm<T> {

  private final String name = "Selection Sort";
  private T min;
  private int minIndex = 0;
  private List<T> list;
  private List<Integer> comparedValues = new ArrayList<>();

  public SelectionOrder(List<T> list, boolean ascendant) {
    super(list, ascendant);
    this.list = super.getList();
  }

  @Override
  public void orderList() {
    T aux;
    int indexAux = 0;
    comparedValues.clear();
    min = list.get(minIndex);
    for (int i = minIndex; i < list.size(); i++) {
      if ((super.cast(list.get(i)) < super.cast(min) && super.getAscendant())
          || (super.cast(list.get(i)) > super.cast(min) && !super.getAscendant())) {
        aux = list.get(i);
        list.set(i, list.get(minIndex));
        list.set(minIndex, aux);
        indexAux = i;
      }
    }
    comparedValues.add(indexAux);
    comparedValues.add(minIndex);
    super.setList(list);
    super.setComparedValues(comparedValues);
    if (minIndex + 1 < list.size()) {
      minIndex++;
    } else {
      minIndex = 0;
    }
  }

  @Override
  public void printPresentation() {
    System.out.println("Ordering with Selection Sort Algorithm: ");
  }

  @Override
  public String getName() {
    return name;
  }
}
