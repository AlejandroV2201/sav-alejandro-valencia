package Algorithms;

public interface IAlgorithm {

  void orderList();

  void checkOrder();

  void printPresentation();
}
