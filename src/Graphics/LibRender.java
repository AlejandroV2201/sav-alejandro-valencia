package Graphics;

import Algorithms.Algorithm;
import Data.ArgsManager;
import Utils.Utils;
import java.util.ArrayList;
import java.util.List;
import sav.RenderOperation;
import sav.SavGenerator;
import sav.SavSettings;
import sav.SwingRenderer;
import sav.VerticalConsoleRenderer;
import sav.HorizontalConsoleRenderer;

public class LibRender<T> implements Render{

  private final ArgsManager data;
  private final Algorithm<Object> orderType;
  private final String libRender;
  public LibRender(ArgsManager data, Algorithm<Object> orderType) {
    this.data = data;
    this.orderType = orderType;
    this.libRender = data.getRender();
  }

  public void printList() {
    SavSettings<T> savSettings = configureLibSettings();
    SavGenerator<T> savGenerator = configureLibGenerator();
    if (libRender.equals("horizontal")) {
      new HorizontalConsoleRenderer<T>().render(savGenerator, savSettings);
    } else if (libRender.equals("vertical")) {
      new VerticalConsoleRenderer<T>().render(savGenerator, savSettings);
    } else {
      new SwingRenderer<T>().render(savGenerator, savSettings);
    }
  }

  private SavSettings<T> configureLibSettings() {
    Character MAXCHAR = 'z';
    Character MINCHAR = 'A';
    Integer MAXINT = 1000;
    Integer MININT = -1000;
    return new SavSettings<>(data.getList(), data.getTime(),
        (data.getTypeArray() == 'c') ? (T) MAXCHAR : (T) MAXINT,
        (data.getTypeArray() == 'c') ? (T) MINCHAR : (T) MININT,
        this::cast,
        data.isAscendant());
  }

  private SavGenerator<T> configureLibGenerator() {
    return new SavGenerator<T>() {
      @Override
      public void sortItems(List<T> list, boolean b, RenderOperation<T> renderOperation) {
        orderType.checkOrder();
        List<Object> lastList = new ArrayList<>(orderType.getList());
        while (!orderType.isOrdered()) {
          orderType.orderList();
          if (!orderType.getList().equals(lastList)) {

            renderOperation.render((List<T>) orderType.getList());
          }
          lastList = new ArrayList<>(orderType.getList());
          orderType.checkOrder();
        }
      }
    };
  }

  private int cast(Object o) {
    if (data.getTypeArray() == 'n') {
      return Utils.getInstance().castObjectToInt((Integer) o);
    } else if (data.getTypeArray() == 'c' && !(o instanceof Integer)){
      return Utils.getInstance().castObjectToInt((Character) o);
    }
    return 0;
  }
}
