package Graphics;

import Algorithms.Algorithm;
import Data.ArgsManager;
import java.util.ArrayList;
import java.util.List;

public class ConsoleManager implements Render{

  private final ArgsManager data;
  private final Algorithm<Object> orderType;
  public ConsoleManager(ArgsManager data, Algorithm<Object> orderType){
    this.data = data;
    this.orderType = orderType;
  }

  public void printList() {
    int repetitions = 0;
    orderType.printPresentation();
    ConsoleRender.getInstance().printResultList(orderType.getList());
    orderType.checkOrder();
    List<Object> lastList = new ArrayList<>(orderType.getList());
    while (!orderType.isOrdered()) {
      orderType.orderList();
      if (!orderType.getList().equals(lastList)) {
        ConsoleRender.getInstance().printList(orderType.getList(), orderType.getComparedValues());
      }
      lastList = new ArrayList<>(orderType.getList());
      orderType.checkOrder();
      repetitions++;
    }
    ConsoleRender.getInstance().printResultList(orderType.getList());
    ConsoleRender.getInstance().printFinalData(data, orderType.getList(), orderType.getName(), repetitions* data.getTime());
  }
}
