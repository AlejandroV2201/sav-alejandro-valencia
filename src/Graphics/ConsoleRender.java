package Graphics;

import Data.ArgsManager;
import java.util.List;

public final class ConsoleRender {

  private static final ConsoleRender instance = new ConsoleRender();
  private ConsoleRender(){
  }

  public static ConsoleRender getInstance() {
   return instance;
  }


  public void printList(List<Object> list, List<Integer> comparedValues) {
    System.out.print("[");
    if (comparedValues.contains(0)) {
      printInRed(list.get(0) + "");
    } else {
      System.out.print(list.get(0));
    }
    for (int index = 1; index < list.size(); index++) {
      System.out.print(", ");
      if (comparedValues.contains(index)) {
        printInRed(list.get(index) + "");
      } else {
        System.out.print(list.get(index));
      }
    }

    System.out.println("]");
  }

  public void printResultList(List<Object> list) {
    System.out.print(ConsoleColors.GREEN.code + "[");
    System.out.print(list.get(0));
    for (int index = 1; index < list.size(); index++) {
      System.out.print(", ");
      System.out.print(list.get(index));
    }
    System.out.println("]" + ConsoleColors.RESET.code);
  }

  public void printFinalData(ArgsManager data, List<Object> result, String orderType, int totalTime) {
    System.out.println();
    System.out.println("Tipo: " + ((data.getTypeArray() == 'n') ? "[Numérico]" : "[Carácter]"));
    System.out.println("Valores: " + data.getList());
    System.out.println("Ordenamiento: " + result);
    System.out.println("Algoritmo: " + orderType);
    System.out.println("Total time: " + totalTime + " ms.");
  }

  private void printInRed(String value) {
    System.out.print(ConsoleColors.RED.code + value + ConsoleColors.RESET.code);
  }

}
