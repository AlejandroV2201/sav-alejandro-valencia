package Graphics;

public enum ConsoleColors {
  RESET("\u001B[0m"),
  GREEN("\u001B[32m"),
  BLACK("\u001B[30m"),
  YELLOW("\u001B[33m"),
  BLUE("\u001B[34m"),
  WHITE("\u001B[37m"),
  RED("\u001B[31m");

  public final String code;
  ConsoleColors(String code) {
    this.code = code;
  }
}
