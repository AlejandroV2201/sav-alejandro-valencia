# SAV - Alejandro Valencia

## Entrada de datos

Actualmente, la entrada de datos sigue el siguiente formato:

#### java SAV a=i t=n o=az in=r r=10 s=140 c=swing

Se requiere el algoritmo, el tipo de entrada, ordenamiento, y datos.

El último argumento es opcional, para elegir el tipo de renderizado
que se va a usar. Por defecto se elige Swing.

## Algoritmos de Ordenamiento Implementados:
- Ordenamiento Burbuja
- Ordenamiento por Inserción
- Ordenamiento por Selección

## Algoritmos de Ordenamiento Misceláneos:
Usados como prueba y con fines de entretener, no tienen eficiencia y no
son recomendables:
- Ordenamiento de Stalin
- Ordenamiento Aleatorio

## Carácteres correspondientes a cada algoritmo:

* Ordenamiento Burbuja: 'b' o 'B'
* Ordenamiento por Inserción 'i' o 'I'
* Ordenamiento por Selección 's' o 'S'
* Ordenamiento de Stalin 't' o 'T'
* Ordenamiento Aleatorio 'r' o 'R'

## Opciones de renderizado disponibles:

* Console: Implementación propia que imprime a través de la consola.
* Horizontal: Render de librería externa que imprime a través de la consola
horizontalmente.
* Vertical: Render de librería externa que imprime a través de la consola
  verticalmente.
* Swing: Render de librería externa que imprime a través de una ventana gráfica.